//
//  AnimationTextViewController.swift
//  Animation
//
//  Created by Martin Steiner on 17.01.19.
//  Copyright © 2019 Martin Steiner. All rights reserved.
//

import UIKit

class AnimationTextViewController: UIViewController {
    
    var stringFromTextViewController: String!
    var labels = [UILabel]()
    
    func doAnimation() {
        for (index, current) in labels.enumerated() {
            let yCenter = view.frame.height/2
            let currentHeightWithOffset = current.frame.height+10
            let factor = CGFloat(index - labels.count/2)
            let y = yCenter + currentHeightWithOffset * factor
            current.transform = CGAffineTransform(translationX: 0, y: y)
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let arr = stringFromTextViewController.split(separator: " ")
        
        for string in arr {
            let animationText = string as NSString
            let label = UILabel()
            label.font = label.font.withSize(30.0)
            let labelSize = animationText.size(withAttributes: [NSAttributedString.Key.font : label.font])
            label.frame = CGRect(x: self.view.frame.width/2 - labelSize.width/2, y: 0, width: labelSize.width, height: labelSize.height)
            label.text = String (string)
            labels.append(label)
            view.addSubview(label)
        }
        
        UIView.animate(withDuration: 2.0, delay: 0.5, options: .curveEaseOut, animations: {
            self.doAnimation()
        })

    }
}
