//
//  TextViewController.swift
//  Animation
//
//  Created by Martin Steiner on 01.01.19.
//  Copyright © 2019 Martin Steiner. All rights reserved.
//

import UIKit

protocol MyTextCellDelegate {
    func myTextCellDelegateDone( index:IndexPath, text: String)
}
 
class MyTextCell: UITableViewCell, UITextFieldDelegate{
    @IBOutlet weak var myTextField: UITextField!
    var index: IndexPath!
    var delegate: MyTextCellDelegate!
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        delegate.myTextCellDelegateDone(index: index, text: myTextField.text!)
        return true
    }
    
    //is called after constructor and storyboardoutlets are loaded
    override func awakeFromNib() {
        myTextField.delegate = self
    }
    

}


class MyLabelCell: UITableViewCell {
    @IBOutlet weak var myLabel: UILabel!
}

class TextViewController: UIViewController , UITableViewDelegate, UITableViewDataSource, MyTextCellDelegate{
    
    @IBOutlet weak var tableView: UITableView!
    var enteredText: [String] = []
    var valueToPass: String!

    func myTextCellDelegateDone(index: IndexPath, text: String) {
        if text.isEmpty {self.showAlertView("Textfield should not be empty", title: "Attention!")}
        enteredText[index.row] = text
        tableView.reloadData()
    }
    
    func showAlertView(_ text: String, title: String){
        let alert = UIAlertController(title: title, message: text, preferredStyle: .alert)
        
        let okay = UIAlertAction(title: "OK", style: .default) {(action) in print("User pressed \(String(describing: action.title)) button")
        }
        
        alert.addAction(okay)
        self.present(alert, animated: true)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
    @IBAction func addButtonPressed(_ sender: UIBarButtonItem) {
        enteredText.append("")
        
        let indexPath = IndexPath(row: enteredText.count - 1, section: 0)
        
        tableView.beginUpdates()
        tableView.insertRows(at: [indexPath], with: .automatic)
        tableView.endUpdates()
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return enteredText.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var result: UITableViewCell?
            if enteredText[indexPath.row] != ""{
                let cell = tableView.dequeueReusableCell(withIdentifier: "myLabelCell", for: indexPath) as! MyLabelCell
                cell.myLabel.text = enteredText[indexPath.row]
                result  = cell
            }else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "myTextCell", for: indexPath) as! MyTextCell
                cell.delegate = self
                cell.index = indexPath
                result = cell
                cell.myTextField.text = ""
            }
        return result!
    }
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        let currentCell = tableView.cellForRow(at: indexPath) as! MyLabelCell
        valueToPass = currentCell.myLabel.text
        
        return indexPath
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "TextToAnimation") {
            let animationVC = segue.destination as! AnimationTextViewController
            animationVC.stringFromTextViewController = valueToPass
        }
    }
    
    
    
    
}
