//
//  RectViewController.swift
//  Animation
//
//  Created by Martin Steiner on 01.01.19.
//  Copyright © 2019 Martin Steiner. All rights reserved.
//

import UIKit

class RectViewController: UIViewController {
    
    var views = [UIView]()
    
    func generateRandomColor() -> UIColor {
        return UIColor(red:   CGFloat(Float.random(in: 0..<1)),
                       green: CGFloat(Float.random(in: 0..<1)),
                       blue:  CGFloat(Float.random(in: 0..<1)),
                       alpha: 1.0)
    }
    
    func generateRandomTransformMulti() -> CGFloat{
        return CGFloat(Float.random(in: -1...1))
    }
    
    func doAnimation() {
        let sb = view.frame
        for view in views {
            view.frame.size.height = 120
            view.frame.size.width = 120
            view.transform = CGAffineTransform(translationX: sb.width/2 * generateRandomTransformMulti(), y: sb.height/2 * generateRandomTransformMulti())
            view.backgroundColor = generateRandomColor()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        let centerX = view.frame.width/2 - 30
        let centerY = view.frame.height/2 - 30
        for _ in 0..<40 {
            let rect = CGRect(x: centerX, y: centerY, width: 60, height: 60)
            let current = UIView(frame: rect)
            current.backgroundColor = generateRandomColor()
            views.append(current)
            view.addSubview(current)
        }
        
        UIView.animate(withDuration: 5.0, delay: 1.0, options: .curveEaseInOut , animations: {
            self.doAnimation()
            })
    }
    
}
